# `gerald`

Analog stick emulation via mouse and keyboard.

Originally imagined in LD51. [This blog post explains how the analog stick emulation worked in that jam entry.](https://ldjam.com/events/ludum-dare/51/alien-cattle-rustlers/deep-dive-on-alien-flight-controls)
