module Gerald.Brick where

import Control.Monad.IO.Class
import GHC.Exts (fromList)
import Data.Functor
import Data.Text.Encoding qualified as TE
import Gerald.Evdev qualified as Evdev
import Evdev qualified
import Brick qualified
import Brick.Widgets.Border.Style qualified as Brick
import Brick.Widgets.List qualified as Brick
import Graphics.Vty qualified as Vty

data Gerald'DeviceSelect = Gerald'DeviceSelect
  { devices :: Brick.List () Evdev.VerboseDevice
  }

data Gerald'Event =
  Gerald'EvdevEvent Evdev.Device Evdev.Event

gerald'DeviceSelect :: Brick.App Gerald'DeviceSelect Gerald'Event ()
gerald'DeviceSelect =
  -- TODO: Get all verbose devices
  -- TODO: Start up BChan to send Gerald'EvdevEvents
  Brick.App
    { appDraw = \Gerald'DeviceSelect{..} ->
        [Brick.hBox
         [ Brick.withBorderStyle Brick.ascii $ Brick.txt "Hello"
         , Brick.renderList
           (\_ Evdev.VerboseDevice{..} -> Brick.txt (TE.decodeUtf8 name))
           True
           devices
         ]
        ]
    , appChooseCursor = \_ _ -> Nothing
    , appHandleEvent = \_ -> pure ()
    , appStartEvent = pure ()
    , appAttrMap = \_ -> Brick.attrMap Vty.defAttr []
    }

runGerald'DeviceSelect :: MonadIO m => m Evdev.VerboseDevice
runGerald'DeviceSelect = liftIO $ do
  let vtyIO = Vty.mkVty Vty.defaultConfig
  vty <- vtyIO
  allDevices <- Evdev.listDevices
  let devices = Brick.list () (fromList allDevices) 1
  glist <- Brick.customMain vty vtyIO Nothing gerald'DeviceSelect Gerald'DeviceSelect {..}
  pure $ maybe (error "nope") snd $ Brick.listSelectedElement $ glist.devices
