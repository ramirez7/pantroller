{-# LANGUAGE NoFieldSelectors #-}

module Gerald.Evdev (module Gerald.Evdev) where

import Prelude hiding (product)

import Evdev
import Evdev.Codes qualified as Evdev
import RawFilePath.Directory
import System.FilePath.ByteString
import System.Posix.Types (Fd)
import System.Posix.IO.ByteString (OpenMode (ReadOnly), OpenFileFlags (nonBlock), defaultFileFlags, openFd)

import Control.Monad.Reader
import Control.Monad.IO.Class
import Data.Function ((&))
import Data.Traversable (for)
import Data.List (isPrefixOf)
import Data.Map (Map)
import Data.Map.Strict qualified as Map
import Data.Monoid (Ap (..))
import Data.ByteString qualified as B

import Data.IORef
import System.IO.Unsafe

deviceSeq :: IORef Int
deviceSeq = unsafePerformIO $ newIORef 0
{-# NOINLINE deviceSeq #-}

-- Dunno if we need this
newNonBlockingDevice :: RawFilePath -> IO Device
newNonBlockingDevice path = newDeviceFromFd =<< openFd path ReadOnly Nothing defaultFileFlags { nonBlock = True }

listDevices :: IO [VerboseDevice]
listDevices = do
  fns <- listDirectory evdevDir
  -- Don't know how standard this is, but on my system, only "eventX" are valid
  -- ls -l /dev/input/by-path shows how devices are mapped to eventXes
  let paths = fmap (evdevDir </>) $ filter (B.isPrefixOf "event") fns
  devices <- traverse newDevice paths
  for (reverse devices) mkVerboseDevice

mkVerboseDevice :: Device -> IO VerboseDevice
mkVerboseDevice device = device & runReaderT do
    name <- ask >>= liftIO . deviceName
    path <- devicePath <$> ask
    properties <- ask >>= liftIO . deviceProperties
    eventTypes <- ask >>= liftIO . deviceEventTypes
    fd  <- ask >>= liftIO . deviceFd
    phys <- ask >>= liftIO . devicePhys
    uniq <- ask >>= liftIO . deviceUniq
    product <- ask >>= liftIO . deviceProduct
    vendor <- ask >>= liftIO . deviceVendor
    bustype <- ask >>= liftIO . deviceBustype
    version <- ask >>= liftIO . deviceVersion
    absAxes <- ask >>= liftIO . deviceAbsAxes
    idx <- liftIO $ atomicModifyIORef' deviceSeq $ \i -> (succ i, i)
    pure VerboseDevice{..}

deviceAbsAxes :: Evdev.Device -> IO (Map Evdev.AbsoluteAxis Evdev.AbsInfo)
deviceAbsAxes device =
  getAp $ flip foldMap [minBound..maxBound] $ \axis -> Ap $ do
    mInfo <- Evdev.deviceAbsAxis device axis
    pure $ maybe mempty (Map.singleton axis) mInfo

data VerboseDevice = VerboseDevice
  { device :: Evdev.Device
  , name :: B.ByteString
  , path :: B.ByteString
  , properties :: [Evdev.DeviceProperty]
  , eventTypes :: [Evdev.EventType]
  , fd :: Fd
  , phys :: Maybe B.ByteString
  , uniq :: Maybe B.ByteString
  , product :: Int
  , vendor :: Int
  , bustype :: Int
  , version :: Int
  , absAxes :: Map Evdev.AbsoluteAxis Evdev.AbsInfo
  , idx :: Int
  }
  deriving stock (Show)
