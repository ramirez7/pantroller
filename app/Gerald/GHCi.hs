module Gerald.GHCi where

import Brick qualified as Brick
import Brick.Forms qualified as Brick
import Graphics.Vty qualified as Vty
import Evdev qualified
import Evdev.Stream qualified as Evdev
import Gerald.Evdev qualified as Evdev
import Haskus.Utils.Variant
import GHC.Generics
import Data.Generics.Product.Fields
import Lens.Micro
import Data.Functor
import Control.Monad
import Streamly.Prelude qualified as Streamly
import Text.Pretty.Simple (pPrint)

viewEvents :: Evdev.Device -> IO ()
viewEvents = Streamly.mapM_ pPrint . Evdev.readEvents

-- OLD: First attempt at something:
brickMain :: IO ()
brickMain = do
  let vtyIO = Vty.mkVty Vty.defaultConfig
  vty <- vtyIO
  void $ Brick.customMain vty vtyIO Nothing gerald'App $ Gerald
    { screen = V MainMenu
    , selectedDevice = Nothing
    }

data Gerald = Gerald
  { screen :: V '[ DeviceSelect, MainMenu ]
  , selectedDevice :: Maybe Evdev.Device
  }

type Gerald'Event = V '[]
type Gerald'Resource = V '[]
type Gerald'App = Brick.App Gerald Gerald'Event Gerald'Resource

data DeviceSelect = DeviceSelect
  { devices :: [Evdev.Device]
  , currentDevice :: Lens' Gerald (Maybe Evdev.Device)
  , deviceList :: Brick.Form Evdev.Device Gerald'Event Gerald'Resource
  }

data MainMenu = MainMenu

gerald'App :: Gerald'App
gerald'App = Brick.App
  { appDraw = \Gerald{..} -> []
  , appChooseCursor = \_ _ -> Nothing
  , appHandleEvent = \case
      _ -> pure ()
  , appStartEvent = pure ()
  , appAttrMap = const $ Brick.attrMap Vty.defAttr []
  }
