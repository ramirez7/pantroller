module Main where

import Control.Monad
import Data.Functor.Identity
import Data.Int
import Data.Map (Map)
import Data.Map.Strict qualified as Map
import Text.Pretty.Simple (pPrint)
import Gerald.GHCi
import Gerald.Evdev
import Evdev qualified
import Evdev.Codes qualified as Evdev
import Evdev.Uinput qualified as Uinput
import Evdev.Stream qualified as Evdev
import System.Environment (getArgs)
import Streamly.Prelude qualified as S
import Linear
import Control.Monad.State.Strict

main :: IO ()
main = getArgs >>= \case
  ["evtest"] -> do
    ds <- listDevices
    pPrint ds
    putStrLn "Choose a device idx: "
    idx <- readLn @Int
    viewEvents (ds !! idx).device
  ["gerald-the-mouse"] -> do
    -- TODO: CLI/file config
    putStrLn "Please press the left mouse button + the spacebar to detect devices"
    putStrLn "(Press ESC for a manual prompt)"
    -- TODO: Use brick here? - it might work nicer
    mDevices :: GeraldTheMouse'Devices Maybe <- S.foldr findDevices gerald'NoDevices Evdev.allEvents
    GeraldTheMouse'Devices{..} :: GeraldTheMouse'Devices Identity <- case mDevices of
      GeraldTheMouse'Devices{mouse = Just mouse, keyboard = Just keyboard} ->
        pure GeraldTheMouse'Devices{..}
      _ -> do
        ds <- listDevices
        pPrint ds
        putStrLn "Choose a mouse idx: "
        mouseIdx <- readLn @Int
        putStrLn "Choose a keyboard idx: "
        keyboardIdx <- readLn @Int
        let mouse = (ds !! mouseIdx).device
        let keyboard = (ds !! keyboardIdx).device
        pure GeraldTheMouse'Devices{..}
    let config = GeraldTheMouse'Config
          { radius = 30
          , deadzone = 15
          , keymappings = Map.fromList
            [ Evdev.KeyF .-> Evdev.BtnA
            , Evdev.KeyD .-> Evdev.BtnB
            , Evdev.KeyA .-> Evdev.BtnZ
            , Evdev.KeyS .-> Evdev.BtnX
            , Evdev.KeyE .-> Evdev.BtnY
            , Evdev.Key1 .-> Evdev.BtnStart
            , Evdev.KeyGrave .-> Evdev.BtnSelect
            , Evdev.KeyW .-> Evdev.BtnC
            , Evdev.KeyQ .-> Evdev.BtnTl
            , Evdev.KeyR .-> Evdev.BtnTr
            -- TODO: Button to toggle the controller on/off. On = grab the mouse and keyboard.
            ]
          }

    let absInfo = Evdev.AbsInfo
          { absValue = 0
          , absMinimum = -255
          , absMaximum = 255
          , absFuzz = 0
          , absFlat = config.deadzone
          , absResolution = 0
          }
    let opts = Uinput.defaultDeviceOpts
          { Uinput.keys = Map.elems config.keymappings
          , Uinput.absAxes = (,absInfo) <$> [Evdev.AbsX, Evdev.AbsY]
          }
    controller <- Uinput.newDevice "gerald-the-mouse" opts
    let initialState = GeraldTheMouse'State False 0
    putStrLn "Running gerald-the-mouse..."
    void $ flip runStateT initialState
      $ S.mapM_ (\evs -> liftIO $ unless (null evs) $ pPrint evs >> Uinput.writeBatch controller evs)
      $ emulateController config
      $ S.liftInner (S.async (Evdev.readEvents mouse) (Evdev.readEvents keyboard)) -- TODO: Also read events from keyboard
  _ -> error "Unknown command"

type family HKD f a where
  HKD Identity a = a
  HKD f a = f a

data GeraldTheMouse'Devices f = GeraldTheMouse'Devices
  { mouse :: HKD f Evdev.Device
  , keyboard :: HKD f Evdev.Device
  }
gerald'NoDevices :: GeraldTheMouse'Devices Maybe
gerald'NoDevices = GeraldTheMouse'Devices Nothing Nothing

-- TODO: Gotta short-circuit on either success or ESC:
findDevices :: (Evdev.Device, Evdev.Event) -> GeraldTheMouse'Devices Maybe -> GeraldTheMouse'Devices Maybe
findDevices _ done@(GeraldTheMouse'Devices Just{} Just{}) = done
findDevices _ _ = error "todo"

data GeraldTheMouse'Config = GeraldTheMouse'Config
  { radius :: Int32
  , deadzone :: Int32
  , keymappings :: Map Evdev.Key Evdev.Key
  }

data GeraldTheMouse'State = GeraldTheMouse'State
  { grabbed :: Bool
  , location :: V2 Float
  } deriving stock (Eq, Show)

(.->) :: a -> b -> (a, b)
(.->) = (,)

emulateController
  :: forall m t
   . MonadState GeraldTheMouse'State m
  => S.IsStream t
  => S.MonadAsync m
  => GeraldTheMouse'Config
  -> t m Evdev.Event
  -> t m [Evdev.EventData]
emulateController GeraldTheMouse'Config{..} = S.mapM $ \e -> case e.eventData of
  Evdev.KeyEvent Evdev.BtnMouse Evdev.Pressed -> do
    modify $ \GeraldTheMouse'State{..} -> GeraldTheMouse'State{grabbed = True, ..}
    pure []
  Evdev.KeyEvent Evdev.BtnMouse Evdev.Released -> do
    put GeraldTheMouse'State{grabbed = False, location = 0}
    pure
     [ Evdev.AbsoluteEvent Evdev.AbsX (Evdev.EventValue 0)
     , Evdev.SyncEvent Evdev.SynReport
     , Evdev.AbsoluteEvent Evdev.AbsY (Evdev.EventValue 0)
     , Evdev.SyncEvent Evdev.SynReport
     ]
  Evdev.RelativeEvent Evdev.RelX (Evdev.EventValue dx) -> do
    GeraldTheMouse'State{..} <- get
    if not grabbed then pure []
      else do
      let dx'normed :: Float = fromIntegral dx / fromIntegral radius
      let loc'updated = location + (V2 dx'normed 0)
      let loc'norm = norm loc'updated
      let loc'normed = if | nearZero loc'updated -> 0
                          | loc'norm > 1 -> (/ loc'norm) <$> loc'updated
                          | otherwise -> loc'updated
      let V2 x'normed _ = loc'normed
      let x'255 = round (x'normed * 255)
      put GeraldTheMouse'State{location = loc'normed, ..}
      pure
        [ Evdev.AbsoluteEvent Evdev.AbsX (Evdev.EventValue x'255)
        , Evdev.SyncEvent Evdev.SynReport
        ]
  Evdev.RelativeEvent Evdev.RelY (Evdev.EventValue dy) -> do
    GeraldTheMouse'State{..} <- get
    if not grabbed then pure []
      else do
      let dy'normed :: Float = fromIntegral dy / fromIntegral radius
      let loc'updated = location + (V2 0 dy'normed)
      let loc'norm = norm loc'updated
      let loc'normed = if | nearZero loc'updated -> 0
                          | loc'norm > 1 -> (/ loc'norm) <$> loc'updated
                          | otherwise -> loc'updated
      let V2 _ y'normed = loc'normed
      let y'255 = round (y'normed * 255)
      put GeraldTheMouse'State{location = loc'normed, ..}
      pure
        [ Evdev.AbsoluteEvent Evdev.AbsY (Evdev.EventValue y'255)
        , Evdev.SyncEvent Evdev.SynReport
        ]
  Evdev.KeyEvent k ke
    | Just k' <- Map.lookup k keymappings ->
        case ke of
          Evdev.Repeated -> pure []
          _ -> pure
            [ Evdev.KeyEvent k' ke
            , Evdev.SyncEvent Evdev.SynReport
            ]
  _ -> pure []
