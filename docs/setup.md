# Setup

## Add yourself to the `input` group

Per [the `evdev` docs](https://hackage.haskell.org/package/evdev#readme):

> Your user will need to be a member of the `input` group in order to read from devices. Try `usermod -a -G input [username]`.

### NixOS

You can accomplish this on NixOS by adding `"input"` to the `users.users.YOUR_USERNAME.extraGroups` list. For example:

```diff
   users.mutableUsers = false;
   users.users.YOUR_USERNAME = {
     isNormalUser = true;
-    extraGroups = [ "wheel" "networkmanager" ];
+    extraGroups = [ "wheel" "networkmanager" "input" ];
     passwordFile = "/etc/passwordFile-YOUR_USERNAME";
```
