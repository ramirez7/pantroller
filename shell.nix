with import ./default.nix;
shellFor {
  tools = {
    cabal = "latest";
    hsc2hs = "latest";
  };

  buildInputs = [ pkgs.sdl-jstest ];
  withHoogle = false;
}
